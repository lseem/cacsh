CC = tcc

CACT_LIB_DIR = /home/luca/builds/cact/lib

default: cacsh

cacsh: cacsh.c ${CACT_LIB_DIR}/libcact.a
	${CC} $^ -o $@ 
