#include <stdio.h>
#include <stdlib.h>		// EXIT_SUCCESS and EXIT_FAILURE and exit(...)
#include <string.h>		// strotok strcmp
#include <unistd.h>		// fork, execvp
#include <sys/wait.h>	// waitpid

#define CACT_RL_BUFSIZE 1024
#define CACT_TOK_BUFSIZE 64
#define CACT_TOK_DELIM " \t\r\n\a"

void cact_loop(void);
char *cact_read_line(void);
char **cact_split_line(char *line);
int cact_launch(char **args);
int cact_execute(char **args);


/* built-in shell commands */
int cact_help(char **args);
int cact_exit(char **args);

char *builtin_str[] = {
	"help",
	"exit"
};

int (*builtin_func[]) (char**) = {
				&cact_help,
				&cact_exit
};

int
cact_num_builtins()
{
	return sizeof(builtin_str)/sizeof(char*);
}


int
main(int argc, char **argv)
{
	cact_loop();

	return EXIT_SUCCESS;
}

void
cact_loop(void)
{
	char *line;
	char **args;
	int status;
	do {
		printf("> ");
		fflush(stdout);
		line = cact_read_line();
		args = cact_split_line(line);
		status = cact_execute(args);	
		free(line);
		free(args);
	} while (status);
}

char
*cact_read_line(void)
{
	int bufsize = CACT_RL_BUFSIZE;
	int position = 0;
	char *buffer = malloc(sizeof(char) * bufsize);
	int c;
	if (!buffer) {
		fprintf(stderr, "cact: allocation error\n");
		exit(EXIT_FAILURE);
	}

	while (1) {
		// read a character
		c = getchar();
		// if we hit newline replace it with a null character and return, if we hit EOF (=crtl+D) exit
		if (c == EOF)	{
			printf("\n");	
			exit(EXIT_SUCCESS);
		}	else if (c == '\n') {
			buffer[position++] = '\0';
			return buffer;
		} else {
			buffer[position] = c;
		}
		position++;

		// if we have exceeded the buffer, reallocate
		if (position > bufsize) {
			bufsize += CACT_RL_BUFSIZE;
			buffer = realloc(buffer, bufsize);
			if (!buffer) {
				fprintf(stderr, "cact: allocation error\n");
				exit(EXIT_FAILURE);
			}
		}
	}
}

char
**cact_split_line(char *line)
{
	int bufsize = CACT_TOK_BUFSIZE, position = 0;
	char **tokens = malloc(bufsize * sizeof(char*));
	char *token;

	if (!tokens) {
		fprintf(stderr, "cact: allocation error\n");
		exit(EXIT_FAILURE);
	}

	token = strtok(line, CACT_TOK_DELIM);
	while (token != NULL) {
		tokens[position] = token;
		position++;;

		if (position >= bufsize) {
				bufsize += CACT_TOK_BUFSIZE;
				tokens = realloc(tokens, bufsize * sizeof(char*));
				if (!tokens) {
					fprintf(stderr, "cact: allocation error\n");
					exit(EXIT_FAILURE);
				}
		}

		token = strtok(NULL, CACT_TOK_DELIM);
	}
	tokens[position] = NULL;
	return tokens;
}

int
cact_launch(char **args)
{
	printf("%s\n", *args);
	return 1;
}

int
cact_help(char **args)
{
	int i;
	printf("cact\nThe following functions are built in:\n");

	for (i = 0; i < cact_num_builtins(); i++)
		printf("\t%s\n", builtin_str[i]); 

	return 1;
}

int
cact_exit(char **args)
{
	return 0;
}

int
cact_execute(char **args)
{
	int i;

	if (args[0] == NULL)
		return 1;			// an empty command was entered

	for (i = 0; i < cact_num_builtins(); i++) {
		if (strcmp(args[0], builtin_str[i]) == 0)
			return (*builtin_func[i])(args);
	}

	return cact_launch(args);
}
